#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re
import subprocess

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
taxo = str(sys.argv[5])

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def get_taxon(taxid, cur):
    cur.execute("SELECT name FROM taxon WHERE taxonid = '"+taxid+"'")
    res = cur.fetchall()
    return res

def get_all_tax(taxid, cur):
    cur.execute("SELECT taxonid FROM taxon WHERE path like '%/"+taxid+"/%'")
    res = cur.fetchall()
    return res

def insert_taxon(taxid, name, path, conn, cur):
    print(taxid)
    insert = "INSERT INTO taxon (taxonid, name, path) VALUES ('"+taxid+"', '"+name+"', '"+path+"')"
    sql.write(insert+";\n")
    cur.execute(insert)
    conn.commit()

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open(taxo, 'r') as taxofile, open('Taxons.sql', 'w') as sql:
    for line in taxofile:
        liste = line.strip().split('\t')
        taxid = liste[0]
        tax = get_taxon(taxid, cur)

        if tax == []:
            all_tax = get_all_tax(taxid, cur)

            if len(all_tax) != 0:
                name = liste[1] ; name = re.sub("'", "''", name)
                path = liste[2]
                insert_taxon(taxid, name, path, conn, cur)
