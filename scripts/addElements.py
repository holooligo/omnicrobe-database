#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re
import subprocess

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
obo_file_habitat = str(sys.argv[5])
path_file_habitat = str(sys.argv[6])
obo_file_phenotype = str(sys.argv[7])
path_file_phenotype = str(sys.argv[8])
obo_file_use = str(sys.argv[9])
path_file_use = str(sys.argv[10])

list_obo = []
list_obo.append(obo_file_habitat) ; list_obo.append(obo_file_phenotype) ; list_obo.append(obo_file_use)
list_paths = []
list_paths.append(path_file_habitat) ; list_paths.append(path_file_phenotype) ; list_paths.append(path_file_use)
list_type = []
list_type.append('habitat') ; list_type.append('phenotype') ; list_type.append('use')

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def get_id(line):
    ident = re.sub("id: ", "", line.strip())
    return ident

def get_name(line):
    name = re.sub("name: ", "", line.strip())
    name = re.sub("'", "''", name)
    return(name)

def get_synonym(line, synonym):
    r = re.search(r"synonym: \"(?P<syn>.+)\"", line.strip())
    syn = r.group("syn")
    syn = re.sub("'", "''", syn)
    if synonym == "":
        synonym = syn
    else:
        synonym = synonym + ", " + syn
    return synonym

def get_path(id, path_file):
    command = "grep '" + id + "$' " + path_file
    #print('Command :', command)
    get = subprocess.Popen(command,stdout=subprocess.PIPE,shell=True)
    #print('Get : ', get)
    path = str(get.communicate()[0]).split('\\t')[1].replace("\\n'", "")
    #print('Path : ', path)
    return path

def insert_element(id, name, synonym, path, type, conn, cur):
    insert = "INSERT INTO element (identifier, name, synonym, path, type)\
VALUES ('"+id+"', '"+name+"', '"+synonym+"', '"+path+"', '"+type+"')"
    sql.write(insert+";\n")
    cur.execute(insert)
    conn.commit()

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open("Elements.sql", "w") as sql:
    for i in range(len(list_type)):
        with open(list_obo[i], 'r') as obo_file:
            ident = ""
            name = ""
            synonym = ""
            path = ""
            check = False
            for line in obo_file:

                # id
                if line.startswith("[Term]") == True:
                    check = True
                elif line.startswith("[Typedef]") == True:
                    check = False
                elif line.startswith("id:") == True and check == True:
                    if ident != "":
                        path = get_path(ident, list_paths[i])
                        insert_element(ident, name, synonym, path, list_type[i], conn, cur)

                        synonym = "" ; name = "" ; path = ""

                    ident = get_id(line)

                # name
                elif line.startswith("name:") == True and check == True:
                    name = get_name(line)

                # synonym
                elif re.search(r"synonym: \".+\"", line) != None and check == True:
                    synonym = get_synonym(line, synonym)

        path = get_path(ident, list_paths[i])
        insert_element(ident, name, synonym, path, list_type[i], conn, cur)
