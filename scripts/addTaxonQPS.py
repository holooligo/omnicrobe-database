#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re
import subprocess

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
name_file = str(sys.argv[5])
taxo = str(sys.argv[6])

addqps = 0

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def get_taxon(taxid, cur):
    cur.execute("SELECT id FROM taxon WHERE taxonid = '" + taxid + "'")
    resulttax = cur.fetchone()
    return resulttax

def get_list_taxon(taxid, cur):
    cur.execute("SELECT id, taxonid, name FROM taxon WHERE path like '%/" + taxid + "/%'")
    resulttax = cur.fetchall()
    return resulttax

def update_taxon(id, cur, conn):
    update = "UPDATE taxon SET qps = 'yes' WHERE id = " + str(id)
    sqlh.write(update + ";\n")
    cur.execute(update)
    conn.commit()

def update_all(cur, conn):
    update = "UPDATE taxon SET qps = 'no' WHERE qps is null"
    sqlh.write(update + ";\n")
    cur.execute(update)
    conn.commit()

def checkRank(res):
    #command = "grep -e '^" + res[1] + "\s' " + taxo + " | awk -F$'\t' '{print $4}'"
    command = "grep -e '^" + res[1] + "\s' " + taxo + " | awk -F'\t' '{print $4}'"
    #print(command)
    get = subprocess.Popen(command,stdout=subprocess.PIPE,shell=True)
    rank =str(get.communicate()[0]).replace("\\n'", "").replace("b'", "")
    #print(rank)
    return rank

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open(name_file, "r") as qps, open("QPS.sql", "w") as sqlh:
	for line in qps.readlines():

		liste = line.strip().split("\t")

		resulttax = get_taxon(liste[1], cur)
		if resulttax != None:
			update_taxon(resulttax[0], cur, conn)
			addqps += 1

		resultlisttax = get_list_taxon(liste[1], cur) #; print(resultlisttax)
		if resultlisttax != None:
			for res in resultlisttax:
				rank = checkRank(res)
				if rank in ('strain', 'no rank', 'subspecies'):
					update_taxon(res[0], cur, conn)
					addqps += 1

	# No QPS
	update_all(cur, conn)
