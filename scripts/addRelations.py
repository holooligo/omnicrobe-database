#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
input_file = str(sys.argv[5])
rtype = str(sys.argv[6])
source = str(sys.argv[7])

taxnul = 0
relationtotal = 0

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def insert_relation(obtid, taxid, formTaxon, formElement, id_source, source, rtype, conn, cur):
    insert = "INSERT INTO relation (id_element, id_taxon, form_taxon, form_element, id_source, source, type)\
VALUES ('"+str(obtid)+"', '"+str(taxid)+"', '"+formTaxon+"', '"+formElement+"', '"+id_source+"', '"+source+"', '"+rtype+"')"
    sql.write(insert+";\n")
    cur.execute(insert)
    conn.commit()

def insert_taxon(taxid, name, path, conn, cur):
    insert = "INSERT INTO taxon (taxonid, name, path) VALUES ('"+taxid+"', '"+name+"', '"+path+"')"
    sql.write(insert+";\n")
    cur.execute(insert)
    conn.commit()

def get_element(obt_ident, rtype, cur):
    cur.execute("SELECT id, name FROM element e WHERE identifier = '"+obt_ident+"' AND type = '"+rtype+"'")
    res_element = cur.fetchone()
    obtid = res_element[0]
    term_e = res_element[1]
    return obtid, term_e

def get_taxon(taxid, cur):
    cur.execute("select id, name from taxon where taxonid = '"+ taxid +"'")
    resulttax = cur.fetchone()
    return resulttax

def clean_form(term, form):
    clean_form = "<em>"+term+"</em> "+form
    clean_form = re.sub("'", "''", clean_form)
    return clean_form

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open(input_file, 'r') as gb, open(rtype+'_'+source+'.sql', 'w') as sql:

    for line in gb.readlines():

        taxid = '' ; obtid = ''
        term_e = '' ; termt = ''

        line = line.strip()
        liste = line.split("\t")

        if source == "PubMed":
            taxonid = liste[3]
            name = liste[6]
            path = liste[7]
        else:
            taxonid = liste[2]
            name = liste[1]
            path = liste[3]

    	# Integration des taxons
        resulttax = get_taxon(taxonid, cur)

        if resulttax == None:

            taxnul = taxnul + 1

            name = re.sub("'", "''", name)
            insert_taxon(taxonid, name, path, conn, cur)

            resulttax2 = get_taxon(taxonid, cur)
            taxid = resulttax2[0]
            termt = resulttax2[1]

        else:
            taxid = resulttax[0]
            termt = resulttax[1]

        if source == "PubMed":

            # Element ID & Name
            obtid, term_e = get_element(liste[8], rtype, cur)

            # Surface forms
            formTaxon = clean_form(termt, liste[5])
            formElement = clean_form(term_e, liste[10])

            # SQL Insert
            insert_relation(obtid, taxid, formTaxon, formElement, liste[1], source, rtype, conn, cur)
            relationtotal += 1

        elif source in ("GenBank", "DSMZ"):

            for i in range(5, len(liste), 4):

                # Element ID & Name
                obtid, term_e = get_element(liste[i], rtype, cur)

                # Surface forms
                formTaxon = clean_form(termt, liste[0])
                formElement = clean_form(term_e, liste[4])

                # SQL Insert
                insert_relation(obtid, taxid, formTaxon, formElement, liste[8], source, rtype, conn, cur)
                relationtotal += 1

        elif source in ("CIRM-BIA", "CIRM-Levures", "CIRM-CFBP"):

            for i in range(5, len(liste), 3):

                # Element ID & Name
                obtid, term_e = get_element(liste[i], rtype, cur)

                # Surface forms
                formTaxon = clean_form(termt, liste[0])
                formElement = clean_form(term_e, liste[4])

                # SQL Insert
                insert_relation(obtid, taxid, formTaxon, formElement, "-", source, rtype, conn, cur)
                relationtotal += 1

print("TAXA :", taxnul)
print("RELATIONs :", relationtotal)
