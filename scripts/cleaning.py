#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])

# Database Connection
conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
cur = conn.cursor()

# Delete
cur.execute("delete from relation")
print("Relations deleted")
cur.execute("delete from element")
print("Habitats, Phenotypes and Uses deleted")
cur.execute("delete from taxon")
print("Taxa deleted")
cur.execute("delete from list_taxon")
cur.execute("delete from list_taxon_phenotype")
cur.execute("delete from list_taxon_use")
cur.execute("delete from list_habitat")
cur.execute("delete from list_phenotype_taxon")
cur.execute("delete from list_use_taxon")
print("Lists deleted")
cur.execute("delete from ontobiotope")
print("Ontobiotope deleted")
cur.execute("delete from ncbi")
print("Taxonomy deleted")
cur.execute("delete from v_source")
print("Versions deleted")

conn.commit()
