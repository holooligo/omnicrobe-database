#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
rtype = str(sys.argv[5])

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def get_all_taxon(cur):
    cur.execute("SELECT id, taxonid, name, path, synonym FROM taxon")
    res = cur.fetchall()
    return res

def get_relation(id, rtype, cur):
    cur.execute("SELECT r.id_source FROM relation r WHERE r.type = '" + rtype + "' AND r.id_taxon IN (select id from taxon where path like '%/" + id + "/%' or taxonid = '" + id + "')")
    res = cur.fetchall()
    return res

def insert_list(rtype, name, path, cur):
    insert = "INSERT INTO list_taxon"
    if rtype != 'habitat':
        insert += "_" + rtype
    insert += " (name, path) VALUES ('"+name+"', '"+path+"')"
    sql.write(insert+";\n")
    cur.execute(insert)
    conn.commit()

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open('List_taxon_'+rtype+'.sql', 'w') as sql:
    all_taxon = get_all_taxon(cur)

    for taxon in all_taxon:
        relations = get_relation(taxon[1], rtype, cur)
        if len(relations) > 0:
            name = taxon[2] ; name = re.sub("'", "''", name)
            path = taxon[3]
            insert_list(rtype, name, path, cur)
            if taxon[4] != None:
                for synonym in taxon[4].split(', '):
                    synonym = re.sub("'", "''", synonym)
                    insert_list(rtype, synonym, path, cur)
