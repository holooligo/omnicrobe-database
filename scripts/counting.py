#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])

# Database Connection
conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
cur = conn.cursor()

# Output
output = open("Resume.txt", "w")

# Queries
output.write("# Relations Habitat\n\n")
cur.execute("select count(*) from relation where type = 'habitat'")
resulthab = cur.fetchone()
output.write("- All : "+str(resulthab[0])+"\n")
cur.execute("select count(*) from relation where source = 'PubMed' and type = 'habitat'")
resulthabpub = cur.fetchone()
output.write("- PubMed : "+str(resulthabpub[0])+"\n")
cur.execute("select count(*) from relation where source = 'CIRM-BIA' and type = 'habitat'")
resulthabcirmbia = cur.fetchone()
cur.execute("select count(*) from relation where source = 'CIRM-Levures' and type = 'habitat'")
resulthabcirmlevures = cur.fetchone()
cur.execute("select count(*) from relation where source = 'CIRM-CFBP' and type = 'habitat'")
resulthabcirmcfbp = cur.fetchone()
output.write("- CIRM-BIA : "+str(resulthabcirmbia[0])+"\n")
output.write("- CIRM-Levures : "+str(resulthabcirmlevures[0])+"\n")
output.write("- CIRM-CFBP : "+str(resulthabcirmcfbp[0])+"\n")
cur.execute("select count(*) from relation where source = 'GenBank' and type = 'habitat'")
resulthabgen = cur.fetchone()
output.write("- GenBank : "+str(resulthabgen[0])+"\n")
cur.execute("select count(*) from relation where source = 'DSMZ' and type = 'habitat'")
resulthabdsmz = cur.fetchone()
output.write("- DSMZ : "+str(resulthabdsmz[0])+"\n\n")

output.write("# Relations Phenotype\n\n")
cur.execute("select count(*) from relation where source = 'PubMed' and type = 'phenotype'")
resultpheno = cur.fetchone()
output.write("- PubMed : "+str(resultpheno[0])+"\n\n")

output.write("# Relations Use\n\n")
cur.execute("select count(*) from relation where source = 'PubMed' and type = 'use'")
resultuse = cur.fetchone()
output.write("- PubMed : "+str(resultuse[0])+"\n\n")

output.write("# EFSA QPS\n\n")
cur.execute("select count(*) from taxon where qps = 'yes'")
resulttax = cur.fetchone()
output.write("- QPS OK : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from taxon where qps = 'no'")
resulttax = cur.fetchone()
output.write("- QPS NOK : "+str(resulttax[0])+"\n\n")

output.write("# Oracles\n\n")
cur.execute("select count(*) from list_taxon")
resulttax = cur.fetchone()
output.write("- Taxon --> Habitat : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from list_habitat")
resulttax = cur.fetchone()
output.write("- Habitat --> Taxon : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from list_taxon_phenotype")
resulttax = cur.fetchone()
output.write("- Taxon --> Phenotype : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from list_phenotype_taxon")
resulttax = cur.fetchone()
output.write("- Phenotype --> Taxon : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from list_taxon_use")
resulttax = cur.fetchone()
output.write("- Taxon --> Use : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from list_use_taxon")
resulttax = cur.fetchone()
output.write("- Use --> Taxon : "+str(resulttax[0])+"\n\n")

output.write("# Ontologies\n\n")
cur.execute("select count(*) from ontobiotope")
resulttax = cur.fetchone()
output.write("- ONTOBIOTOPE : "+str(resulttax[0])+"\n")
cur.execute("select count(*) from ncbi")
resulttax = cur.fetchone()
output.write("- TAXONOMY : "+str(resulttax[0])+"\n\n")
