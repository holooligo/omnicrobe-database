#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re

count = 0

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
version_onto = str(sys.argv[3])
name_file_habitat = str(sys.argv[4])
name_file_phenotype = str(sys.argv[5])
name_file_use = str(sys.argv[6])
passwd = str(sys.argv[7])
user = str(sys.argv[8])

conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
cur = conn.cursor()

sql = open("Ontobiotope.sql", "w")

# Habitat
onto = open(name_file_habitat, "r")
for line in onto.readlines():

	line = line.strip()
	liste = line.split("\t")

	name = re.sub("'", "''", liste[0])

	insert = "INSERT INTO ontobiotope (name, path, version) VALUES ('"+name+"', '"+liste[1]+"', '"+version_onto+"')"
	sql.write(insert+";\n")
	cur.execute(insert)
	conn.commit()
onto.close()

# Phenotype
onto = open(name_file_phenotype, "r")
for line in onto.readlines():

	line = line.strip()
	liste = line.split("\t")

	name = re.sub("'", "''", liste[0])

	insert = "INSERT INTO ontobiotope (name, path, version) VALUES ('"+name+"', '"+liste[1]+"', '"+version_onto+"')"
	sql.write(insert+";\n")
	cur.execute(insert)
	conn.commit()
onto.close()

# Use
onto = open(name_file_use, "r")
for line in onto.readlines():

	line = line.strip()
	liste = line.split("\t")

	name = re.sub("'", "''", liste[0])

	insert = "INSERT INTO ontobiotope (name, path, version) VALUES ('"+name+"', '"+liste[1]+"', '"+version_onto+"')"
	sql.write(insert+";\n")
	cur.execute(insert)
	conn.commit()
onto.close()

sql.close()
