#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

/* Relations */
CREATE TABLE taxon
(
 id serial unique not null,
 taxonid varchar(20) not null,
 name text not null,
 path text not null,
 synonym text,
 qps varchar(3),
 diffusion varchar(30),
 primary key (id)
);

CREATE TABLE element
(
 id serial unique not null,
 identifier varchar(20) not null,
 name text not null,
 synonym text,
 path text not null,
 type text not null,
 primary key(id)
);

CREATE TABLE relation
(
 id_taxon int not null,
 id_element int not null,
 form_taxon text not null,
 form_element text not null,
 type varchar(50),
 id_source text not null,
 source varchar(50),
 foreign key(id_element) references element(id) on delete CASCADE,
 foreign key(id_taxon) references taxon(id) on delete CASCADE
);

/* Oracles */
CREATE TABLE list_taxon
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

CREATE TABLE list_habitat
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

CREATE TABLE list_taxon_phenotype
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

CREATE TABLE list_phenotype_taxon
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

CREATE TABLE list_taxon_use
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

CREATE TABLE list_use_taxon
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null
);

/* OntoBiotope */
CREATE TABLE ontobiotope
(
 id serial unique not null,
 name varchar(100) not null,
 path text not null,
 version varchar(30) not null,
 primary key(id)
);

/* Taxonomy */
CREATE TABLE ncbi
(
 id serial unique not null,
 name varchar(200) not null,
 path text not null,
 version varchar(50) not null,
 primary key(id)
);

/* Versions */
CREATE TABLE v_source
(
 id serial unique not null,
 omnicrobe varchar(50) not null,
 taxonomy varchar(50) not null,
 ontobiotope varchar(50) not null,
 pubmed varchar(50) not null,
 dsmz varchar(50) not null,
 genbank varchar(50) not null,
 cirmbia varchar(50) not null,
 cirmlevures varchar(50) not null,
 cirmcfbp varchar(50) not null
);

/* Grant */
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE taxon TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE element TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE relation TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_taxon TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_habitat TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_taxon_phenotype TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_phenotype_taxon TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_taxon_use TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE list_use_taxon TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE ontobiotope TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE ncbi TO omi_admin;
GRANT SELECT, DELETE, INSERT, UPDATE ON TABLE v_source TO omi_admin;
GRANT SELECT ON TABLE taxon TO omi_user;
GRANT SELECT ON TABLE element TO omi_user;
GRANT SELECT ON TABLE relation TO omi_user;
GRANT SELECT ON TABLE list_taxon TO omi_user;
GRANT SELECT ON TABLE list_habitat TO omi_user;
GRANT SELECT ON TABLE list_taxon_phenotype TO omi_user;
GRANT SELECT ON TABLE list_phenotype_taxon TO omi_user;
GRANT SELECT ON TABLE list_taxon_use TO omi_user;
GRANT SELECT ON TABLE list_use_taxon TO omi_user;
GRANT SELECT ON TABLE ontobiotope TO omi_user;
GRANT SELECT ON TABLE ncbi TO omi_user;
GRANT SELECT ON TABLE v_source TO omi_user;

/* Index */
CREATE INDEX element_name ON element USING btree (name);
CREATE INDEX element_path_identifier ON element USING btree (path, identifier);
CREATE INDEX taxon_path_taxonid ON taxon USING btree (path, taxonid);
CREATE INDEX taxon_qps ON taxon USING btree (qps);
CREATE INDEX relation_idelement ON relation USING btree (id_element);
CREATE INDEX relation_idtaxon ON relation USING btree (id_taxon);
CREATE INDEX relation_type ON relation USING btree (type);
CREATE INDEX relation_source ON relation USING btree (source);
