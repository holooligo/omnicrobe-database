#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

# Configuration File
configfile: "config.yaml"

# Main rule
rule all:
    input:
        "Resume.txt"

# Rule Elements
rule elements:
    input:
        config['H_ONTO'],
        config['H_PATH'],
        config['P_ONTO'],
        config['P_PATH'],
        config['U_ONTO'],
        config['U_PATH']
    output:
        "Elements.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addElements.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_ONTO]} {config[H_PATH]} {config[P_ONTO]} {config[P_PATH]} {config[U_ONTO]} {config[U_PATH]}"

# Rule PubMed Habitat
rule habitat_pubmed:
    input:
        config['H_PUBMED'],
        "Elements.sql"
    output:
        "habitat_PubMed.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_PUBMED]} habitat PubMed"

# Rule PubMed Phenotype
rule phenotype_pubmed:
    input:
        config['P_PUBMED'],
        "habitat_PubMed.sql"
    output:
        "phenotype_PubMed.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[P_PUBMED]} phenotype PubMed"

# Rule PubMed Use
rule use_pubmed:
    input:
        config['U_PUBMED'],
        "phenotype_PubMed.sql"
    output:
        "use_PubMed.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[U_PUBMED]} use PubMed"

# Rule GenBank Habitat
rule habitat_genbank:
    input:
        config['H_GENBANK'],
        "use_PubMed.sql"
    output:
        "habitat_GenBank.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_GENBANK]} habitat GenBank"

# Rule DSMZ Habitat
rule habitat_dsmz:
    input:
        config['H_DSMZ'],
        "habitat_GenBank.sql"
    output:
        "habitat_DSMZ.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_DSMZ]} habitat DSMZ"

# Rule CIRM-BIA Habitat
rule habitat_cirmbia:
    input:
        config['H_CIRMBIA'],
        "habitat_DSMZ.sql"
    output:
        "habitat_CIRM-BIA.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_CIRMBIA]} habitat CIRM-BIA"

# Rule CIRM-CFBP Habitat
rule habitat_cirmcfbp:
    input:
        config['H_CIRMCFBP'],
        "habitat_CIRM-BIA.sql"
    output:
        "habitat_CIRM-CFBP.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_CIRMCFBP]} habitat CIRM-CFBP"

# Rule CIRM-Levures Habitat
rule habitat_cirmlevures:
    input:
        config['H_CIRMLEVURES'],
        "habitat_CIRM-CFBP.sql"
    output:
        "habitat_CIRM-Levures.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addRelations.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[H_CIRMLEVURES]} habitat CIRM-Levures"

# Rule Taxons
rule taxons:
    input:
        "habitat_CIRM-Levures.sql",
        config['TAXO']
    output:
        "Taxons.sql"
    params:
        queue="long.q"
    shell:
        "python3 scripts/checkTaxon.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[TAXO]}"

# Rule EFSA QPS
rule qps:
    input:
        config['QPS'],
        "Taxons.sql"
    output:
        "QPS.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addTaxonQPS.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[QPS]} {config[TAXO]}"

# Rule List Habitat
rule list_habitat:
    input:
        "QPS.sql"
    output:
        "List_habitat.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListElement.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} habitat"

# Rule List Phenotype
rule list_phenotype:
    input:
        "List_habitat.sql"
    output:
        "List_phenotype.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListElement.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} phenotype"

# Rule List Use
rule list_use:
    input:
        "List_phenotype.sql"
    output:
        "List_use.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListElement.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} use"

# Rule List Taxon Habitat
rule list_taxon_habitat:
    input:
        "List_use.sql"
    output:
        "List_taxon_habitat.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListTaxon.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} habitat"

# Rule List Taxon Phenotype
rule list_taxon_phenotype:
    input:
        "List_taxon_habitat.sql"
    output:
        "List_taxon_phenotype.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListTaxon.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} phenotype"

# Rule List Taxon Use
rule list_taxon_use:
    input:
        "List_taxon_phenotype.sql"
    output:
        "List_taxon_use.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/addListTaxon.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} use"

# Rule Ontobiotope
rule ontobiotope:
    input:
        config['H_PATH'],
        config['P_PATH'],
        "List_taxon_use.sql"
    output:
        "Ontobiotope.sql"
    params:
        queue="short.q"
    shell:
        "python3 scripts/ontobiotope.py {config[DB]} {config[SERVER]} '{config[V_ONTOLOGY]}' {config[H_PATH]} {config[P_PATH]} {config[U_PATH]} {config[PASSWD]} {config[USER]}"

# Rule Version
rule version:
    input:
        "Ontobiotope.sql"
    output:
        "Versions.txt"
    params:
        queue="short.q"
    shell:
        "python3 scripts/versions.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]} {config[V_OMNICROBE]} {config[V_TAXONOMY]} {config[V_ONTOLOGY]} {config[V_PUBMED]} {config[V_DSMZ]} {config[V_GENBANK]} {config[V_CIRMBIA]} {config[V_CIRMLEVURES]} {config[V_CIRMCFBP]}"

# Rule Resume
rule counting:
    input:
        "Ontobiotope.sql",
        "Versions.txt"
    output:
        "Resume.txt"
    params:
        queue="short.q"
    shell:
        "python3 scripts/counting.py {config[DB]} {config[SERVER]} {config[PASSWD]} {config[USER]}"
